package com.apirest.modelo;

public class UsuarioModelo {

    private String userId;

    public UsuarioModelo(){}

    public String getUserId() {
        return userId;
    }

    public UsuarioModelo(String userId) {
        this.userId = userId;
    }
}
