package com.apirest.controlador;

import com.apirest.modelo.ProductoModelo;
import com.apirest.modelo.ProductoPrecioOnly;
import com.apirest.servicio.ProductoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${url.base}")
public class ProductoControlador {

    @Autowired
    private ProductoServicio productoServicio;

    @GetMapping("/productos")
    public List<ProductoModelo> getProductos() {
        return productoServicio.getProductos();
    }

   @GetMapping("/productos/{id}")
    public ResponseEntity getProductoId(@PathVariable int id) {
       ProductoModelo pr = productoServicio.getProducto(id);
       if (pr == null) {
           return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
       }
       return ResponseEntity.ok(pr);
   }

    @PostMapping("/productos")
    public ResponseEntity<String> addProducto(@RequestBody ProductoModelo productoModel) {
        productoServicio.addProducto(productoModel);
        return new ResponseEntity<>("Producto fue creado exitosamente!", HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable int id,
                                         @RequestBody ProductoModelo productToUpdate) {
        ProductoModelo pr = productoServicio.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoServicio.updateProducto(id-1, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable Integer id) {
        ProductoModelo pr = productoServicio.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoServicio.removeProducto(id - 1);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(
            @RequestBody ProductoPrecioOnly productoPrecioOnly, @PathVariable int id){
        ProductoModelo pr = productoServicio.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setPrecio(productoPrecioOnly.getPrecio());
        productoServicio.updateProducto(id-1, pr);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable int id){
        ProductoModelo pr = productoServicio.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsers()!=null)
            return ResponseEntity.ok(pr.getUsers());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
